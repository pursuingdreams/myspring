package org.lcf.spring;
    
/**
 * @description  
 * @ClassName：BeanPostProcessor
 * @author lcf
 * @Date  2022/4/5 19:59
 * @version 1.00
 */
public interface BeanPostProcessor {
    public Object postProcessBeforeInitialization(String beanName, Object bean);
    public Object postProcessAfterInitialization(String beanName, Object bean);
}
