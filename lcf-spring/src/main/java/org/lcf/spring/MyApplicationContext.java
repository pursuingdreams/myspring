package org.lcf.spring;

import org.lcf.spring.annotation.Autowired;
import org.lcf.spring.annotation.Component;
import org.lcf.spring.annotation.ComponentScan;
import org.lcf.spring.annotation.Scope;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @description  
 * @ClassName：MyApplicationContext
 * @author lcf
 * @Date  2022/4/5 19:42
 * @version 1.00
 */
public class MyApplicationContext {

    /**
     * beanName -> BeanDefinition
     */
    private final Map<String,BeanDefinition> beanDefinitionMap = new ConcurrentHashMap<>();
    /**
     * beanName -> bean
     */
    private final Map<String,Object> singletonObjects = new ConcurrentHashMap<>();
    private final List<BeanPostProcessor> beanPostProcessorList = new ArrayList<>();

    // Spring启动流程
    public MyApplicationContext(Class configClass) {
        // 扫描配置类 得到 BeanDefinition
        scan(configClass);
        // 实例化非懒加载单例bean
        //   1. 实例化
        //   2. 属性填充
        //   3. Aware回调
        //   4. 初始化
        //   5. 添加到单例池
        instanceSingletonBean();
    }

    /**
     * 实例化非懒加载 单例bean
     */
    private void instanceSingletonBean() {

        for (String beanName : beanDefinitionMap.keySet()) {
            BeanDefinition beanDefinition = beanDefinitionMap.get(beanName);
            if (ScopeEnum.singleton.name().equals(beanDefinition.getScope())) {
                // 创建bean
                Object bean = doCreateBean(beanName,beanDefinition);
                // 5. 添加到单例池
                singletonObjects.put(beanName,bean);
            }
        }
    }

    /**
     * 根据beanName和beanDefinition创建单例bean
     * @param beanName
     * @param beanDefinition
     * @return
     */
    private Object doCreateBean(String beanName, BeanDefinition beanDefinition) {

        Class clazz = beanDefinition.getBeanClass();
        try {
            // 1. 实例化bean
            Constructor constructor = clazz.getDeclaredConstructor();
            Object instance = constructor.newInstance();

            // 2. 属性填充
            Field[] fields = clazz.getDeclaredFields();
            for (Field field: fields) {
                // 如果字段有Autowired注解，说明该属性是从容器中获取的
                if (field.isAnnotationPresent(Autowired.class)) {
                    String fieldName = field.getName();
                    Object bean = getBean(fieldName);
                    // 将自动注入的bean设置到改属性上
                    field.setAccessible(true);
                    field.set(instance,bean);
                }
            }

            // 3. Aware回调
            if (instance instanceof BeanNameAware) {
                ((BeanNameAware) instance).setBeanName(beanName);
            }
            // 4. 初始化
            if (instance instanceof InitializingBean) {
                ((InitializingBean) instance).afterPropertiesSet();
            }

            // 进行后置处理，用于扩展
            for (BeanPostProcessor beanPostProcessor : beanPostProcessorList) {
                System.out.print("创建"+beanName+"中,对"+beanName+"进行后置处理   ");
                instance = beanPostProcessor.postProcessAfterInitialization(beanName,instance);
            }
            
            return instance;

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 根据beanName获取bean
     * @param beanName
     * @return
     */
    public Object getBean(String beanName) {
        if (singletonObjects.containsKey(beanName)) {
            return singletonObjects.get(beanName);
        } else {
            BeanDefinition beanDefinition = beanDefinitionMap.get(beanName);
            return doCreateBean(beanName,beanDefinition);
        }
    }

    /**
     * 扫描class，转化为BeanDefinition对象，最后添加到beanDefinitionMap中
     * @param configClass
     */
    private void scan(Class configClass) {
        if (configClass.isAnnotationPresent(ComponentScan.class)) {
            ComponentScan componentScanAnnotation  = (ComponentScan) configClass.getAnnotation(ComponentScan.class);
            //org.lcf.client.service
            String packagePath = componentScanAnnotation.value();
            System.out.println("扫描路径:"+packagePath);
            // 扫描包路径得到classList
            List<Class> classList = genBeanClasses(packagePath);
            // 遍历class得到BeanDefinition
            for (Class clazz : classList) {
                // 如果类上面有Component这类注解，就需要将类加入容器，所以要先添加BeanDefinition对象到Map中
                if (clazz.isAnnotationPresent(Component.class)) {

                    BeanDefinition beanDefinition = new BeanDefinition();
                    beanDefinition.setBeanClass(clazz);
                    Component component = (Component) clazz.getAnnotation(Component.class);
                    // Component注解上获取beanName,如果无则默认设置为类名
                    String beanName = component.value();
                    if (beanName.isEmpty()) {
                        beanName = clazz.getSimpleName();
                    }

                    // 如果实现了BeanPostProcessor接口，则往容器中添加BeanPostProcessor后置处理器
                    // A.isAssignableFrom(B) : B类可以强转为A 即B是A的子类或同类
                    if(BeanPostProcessor.class.isAssignableFrom(clazz)) {
                        try {
                            BeanPostProcessor instance = (BeanPostProcessor) clazz.getDeclaredConstructor().newInstance();
                            beanPostProcessorList.add(instance);
                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        }
                    }

                    // 处理类上的Scope注解: 单例、原型
                    if (clazz.isAnnotationPresent(Scope.class)) {
                        Scope scope = (Scope) clazz.getAnnotation(Scope.class);
                        String scopeValue = scope.value();
                        if (ScopeEnum.singleton.name().equals(scopeValue)) {
                            beanDefinition.setScope(ScopeEnum.singleton);
                        } else {
                            beanDefinition.setScope(ScopeEnum.prototype);
                        }
                    }else {
                        // 默认为单例模式
                        beanDefinition.setScope(ScopeEnum.singleton);
                    }

                    beanDefinitionMap.put(beanName,beanDefinition);

                }
            }

        }
    }

    /**
     *
     * @param packagePath = org.lcf.client.service
     * @return
     */
    private List<Class> genBeanClasses(String packagePath) {
        List<Class> classList = new ArrayList<>();
        String packageBegin = packagePath.split("\\.")[0];
        // 注意String类型修改之后会产生了新的对象
        packagePath = packagePath.replace('.','/');
        URL resource = MyApplicationContext.class.getClassLoader().getResource(packagePath);
        File file = new File(resource.getFile());

        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f : files) {
                // E:\code\myspring\lcf-spring\target\classes\org\lcf\client\service\TestService.class
                String fileName = f.getAbsolutePath();
                if (fileName.endsWith(".class")) {
                    String className = fileName.substring(fileName.indexOf(packageBegin), fileName.indexOf(".class"));
                    // org.lcf.client.service.TestService
                    className = className.replace('\\', '.');
                    System.out.println("扫描组件:"+className);
                    Class<?> clazz = null;
                    try {
                        clazz = MyApplicationContext.class.getClassLoader().loadClass(className);
                        classList.add(clazz);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }

                }
            }
        }

        return classList;

    }

}
