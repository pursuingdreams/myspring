package org.lcf.spring;
    
/**
 * @description  
 * @ClassName：InitializingBean
 * @author lcf
 * @Date  2022/4/5 21:38
 * @version 1.00
 */
public interface InitializingBean {
    public void afterPropertiesSet();
}
