package org.lcf.spring;
    
/**
 * @description  
 * @ClassName：ScopeEnum
 * @author lcf
 * @Date  2022/4/5 19:54
 * @version 1.00
 */
public enum ScopeEnum {

    /**
     * 单例模式
     */
    singleton,
    /**
     * 原型模式
     */
    prototype;

}
