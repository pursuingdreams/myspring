package org.lcf.spring.annotation;

import org.lcf.spring.ScopeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description  
 * @ClassName：Scope
 * @author lcf
 * @Date  2022/4/5 20:52
 * @version 1.00
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Scope {
    String value() default "singleton";
}
