package org.lcf.spring;
    
/**
 * @description  
 * @ClassName：BeanNameAware
 * @author lcf
 * @Date  2022/4/5 21:35
 * @version 1.00
 */
public interface BeanNameAware {
    public void setBeanName(String beanName);
}
