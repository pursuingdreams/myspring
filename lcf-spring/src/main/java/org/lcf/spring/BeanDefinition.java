package org.lcf.spring;
    
/**
 * @description  
 * @ClassName：BeanDefinition
 * @author lcf
 * @Date  2022/4/5 19:50
 * @version 1.00
 */
public class BeanDefinition {

    private Class beanClass;

    private ScopeEnum scope;

    public Class getBeanClass() {
        return beanClass;
    }

    public void setBeanClass(Class beanClass) {
        this.beanClass = beanClass;
    }

    public ScopeEnum getScope() {
        return scope;
    }

    public void setScope(ScopeEnum scope) {
        this.scope = scope;
    }
}
