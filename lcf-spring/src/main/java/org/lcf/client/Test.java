package org.lcf.client;

import org.lcf.client.service.TestService;
import org.lcf.spring.MyApplicationContext;

/**
 * @description  
 * @ClassName：Test
 * @author lcf
 * @Date  2022/4/5 19:40
 * @version 1.00
 */
public class Test {

    public static void main(String[] args) {
        MyApplicationContext applicationContext = new MyApplicationContext(MyConfig.class);
        TestService bean = (TestService)applicationContext.getBean("testService");
        bean.test();
    }

}
