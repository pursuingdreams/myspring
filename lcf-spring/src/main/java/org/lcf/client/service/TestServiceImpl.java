package org.lcf.client.service;

import org.lcf.client.service.TestAutowired;
import org.lcf.client.service.TestService;
import org.lcf.spring.annotation.Autowired;
import org.lcf.spring.annotation.Component;

/**
 * @description  
 * @ClassName：TestServiceImpl
 * @author lcf
 * @Date  2022/4/5 21:50
 * @version 1.00
 */
@Component("testService")
public class TestServiceImpl implements TestService {

    @Autowired
    private TestAutowired testAutowired;

    @Override
    public void test() {
        System.out.println("Test");
    }
}
