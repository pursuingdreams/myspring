package org.lcf.client.service;

import org.lcf.spring.annotation.Component;

/**
 * @description  
 * @ClassName：TestService
 * @author lcf
 * @Date  2022/4/5 20:15
 * @version 1.00
 */
public interface TestService {
    public void test();
}
