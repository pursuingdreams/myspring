package org.lcf.client.service;

import org.lcf.spring.BeanPostProcessor;
import org.lcf.spring.annotation.Component;

/**
 * @description  
 * @ClassName：MyBeanPostProcessor
 * @author lcf
 * @Date  2022/4/5 21:57
 * @version 1.00
 */
@Component("myBeanPostProcessor")
public class MyBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(String beanName, Object bean) {
        System.out.println("postProcessBeforeInitialization");
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(String beanName, Object bean) {
        System.out.println("执行初始化后置处理操作");
        return bean;
    }
}
