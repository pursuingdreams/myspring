package org.lcf.client.service;

import org.lcf.spring.annotation.Component;

/**
 * @description  
 * @ClassName：TestAutowired
 * @author lcf
 * @Date  2022/4/5 21:51
 * @version 1.00
 */
@Component("testAutowired")
public class TestAutowired {
}
