package org.lcf.client;

import org.lcf.spring.annotation.ComponentScan;

/**
 * @description  
 * @ClassName：MyConfig
 * @author lcf
 * @Date  2022/4/5 19:44
 * @version 1.00
 */
@ComponentScan("org.lcf.client.service")
public class MyConfig {
}
